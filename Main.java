import GUI.MultiKeyTreasureGUI;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 *
 *
 * Classe Main
 *
 *
 */
public class Main {
    public static void main (String[] args) {

        try {

            String[][] s = new String[10][6];
            Fitxer f = new Fitxer();
            f.llegeixFitxer(args[0]);
            f.trobaEntrada();
            if(args[2].equals("1")) {
                if (args[1].equals("BTSM")) {
                    //Backtracking sense millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    Backtracking b = new Backtracking(f, m, 1);
                    m.startChronometer();
                    b.obrirTresor(0, f.getK1(), f.getK2());
                    ArrayList<Casella> sol = b.getV().getxMillor();

                    for (int i = 0; i < sol.size(); i++) {
                        m.addToPath(sol.get(i).getFila(), sol.get(i).getColumna());
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    m.setPathLength(b.getV().getDistMillor());
                    m.setKeysCollected(b.getV().getClausMillor());
                    m.stopChronometer();

                } else if (args[1].equals("BTAM")) {
                    //Backtracking amb millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    BacktrackingM b = new BacktrackingM(f, m, 1);
                    m.startChronometer();
                    b.obrirTresor(0, f.getK1(), f.getK2());
                    ArrayList<Casella> sol = b.getV().getxMillor();

                    for (int i = 0; i < sol.size(); i++) {
                        m.addToPath(sol.get(i).getFila(), sol.get(i).getColumna());
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    m.setPathLength(b.getV().getDistMillor());
                    m.setKeysCollected(b.getV().getClausMillor());
                    m.stopChronometer();

                } else if(args[1].equals("BBSM")){
                    //B&B sense millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    m.startChronometer();
                    BranchAndBound b = new BranchAndBound(f, m, 1);
                    Configuracio x = b.obrirTresor();
                    for (Casella cas: x.x) {
                        m.addToPath(cas.getFila(),cas.getColumna());

                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    m.stopChronometer();

                } else if (args[1].equals("BBAM")) {
                    //B&B amb millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    BranchAndBoundM b = new BranchAndBoundM(f, m, 1);
                    m.startChronometer();
                    Configuracio x = b.obrirTresor();
                    for (Casella cas: x.x) {
                        m.addToPath(cas.getFila(),cas.getColumna());

                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    m.setKeysCollected(x.clausActuals);
                    m.setPathLength(x.distanciaCami);
                    m.stopChronometer();
                } else {
                    System.out.println("Error en la petició de la variant del joc.");
                }
            } else if(args[2].equals("0")) {
                if (args[1].equals("BTSM")) {
                    //Backtracking sense millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    Backtracking b = new Backtracking(f, m, 0);
                    m.startChronometer();
                    b.iniciaTemps();
                    b.obrirTresor(0, f.getK1(), f.getK2());
                    m.stopChronometer();
                    ArrayList<Casella> sol = b.getV().getxMillor();
                    m.setKeysCollected(b.getV().getClausMillor());
                    m.setPathLength(b.getV().getDistMillor());



                } else if (args[1].equals("BTAM")) {
                    //Backtracking amb millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    BacktrackingM b = new BacktrackingM(f, m, 0);
                    m.startChronometer();
                    b.iniciaTemps();
                    b.obrirTresor(0, f.getK1(), f.getK2());
                    m.stopChronometer();
                    ArrayList<Casella> sol = b.getV().getxMillor();
                    m.setKeysCollected(b.getV().getClausMillor());
                    m.setPathLength(b.getV().getDistMillor());

                } else if(args[1].equals("BBSM")){
                    //B&B sense millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    BranchAndBound b = new BranchAndBound(f, m, 0);
                    m.startChronometer();
                    b.iniciaTemps();
                    Configuracio x = b.obrirTresor();
                    m.stopChronometer();


                } else if (args[1].equals("BBAM")) {
                    //B&B amb millores
                    MultiKeyTreasureGUI m = new MultiKeyTreasureGUI(350, 300,"MultiKeyTreasure", f.getMapaScenario());
                    BranchAndBoundM b = new BranchAndBoundM(f, m, 0);
                    m.startChronometer();
                    b.iniciaTemps();
                    Configuracio x = b.obrirTresor();
                    m.stopChronometer();

                } else {
                    System.out.println("Error en la petició de la variant del joc.");
                }
            }



        } catch (IOException e) {

            System.out.println("Error lectura fitxer");
        }
    }
}

