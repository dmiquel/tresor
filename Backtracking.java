import GUI.MultiKeyTreasureGUI;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * Backtracking
 *
 * Classe que s'encarrega de l'algorisme de Backtracking
 * sense millores
 */


public class Backtracking {

    /* Atributs */
    private ArrayList<Casella> configuracio;
    private ArrayList<boolean[]> passat;
    private Fitxer mapa;
    private MillorSolucio v;
    int modeVista;
    private MultiKeyTreasureGUI m;
    private MultiKeyTreasureGUI m2;
    private DecimalFormat formatter2D;
    private DecimalFormat formatter3D;
    private long startedAt;
    private long stopAt;

    public Backtracking(Fitxer mapa, MultiKeyTreasureGUI m, int modeVista) {
        boolean[] aux;
        this.mapa = mapa;
        this.m = m;
        this.modeVista = modeVista;
        if(modeVista == 0) {
            m2 = new MultiKeyTreasureGUI(350, 300, "M2", mapa.getMapaScenario());
        }
        configuracio = new ArrayList<Casella>();
        //Creem la matriu de booleans i la configuracio
        passat = new ArrayList<boolean[]>();
        for (int i = 0; i < mapa.getDimensioFiles(); i++) {
            aux = new boolean[mapa.getDimensioColumnes()];
            for (int j = 0; j < mapa.getDimensioColumnes(); j++) {
                aux[j] = false;
            }
            passat.add(i,aux);
        }
        v = new MillorSolucio();
        this.formatter2D = new DecimalFormat("00");
        this.formatter3D = new DecimalFormat("000");
    }

    public void obrirTresor(int k, int k1, int k2) {
        int direccio = 0;
        while (direccio < 4) {
            //seguentGerma
            direccio += 1;
            if (solucio(k1, k2)) {
                if (factible())  {
                    tractarSolucio(direccio);

                }

            } else {
                if (completable(k1, k2, direccio)) {
                    passat.get(k1)[k2] = true;
                    if(modeVista == 0) {
                        //indiquem que hem passat per la casella
                        m.addToPath(k1, k2);
                        try {
                          Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    Casella c = new Casella();
                    int num_claus = 0;
                    int M = mapa.getDimensioColumnes();
                    int N = mapa.getDimensioFiles();
                    int aux;
                    for(int i = 0; i < N; i++) {
                        for(int j = 0; j < M; j++) {
                            //Si hem passat per la casella, no es ni el tresor ni l'entrada, sumem les claus
                            if(mapa.getMapa().get(i)[j] != 'T' && mapa.getMapa().get(i)[j] != 'E' && passat.get(i)[j] == true) {
                                aux = Character.getNumericValue(mapa.getMapa().get(i)[j]);
                                num_claus += aux;
                            }
                        }
                    }
                    switch (direccio) {

                        case 1:
                            if(modeVista == 0) {
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            c.setColumna(k2+1);
                            c.setFila(k1);
                            if(mapa.getMapa().get(k1)[k2+1] != 'T' && mapa.getMapa().get(k1)[k2+1] != 'E') {
                                c.setNumClaus(mapa.getMapa().get(k1)[k2+1]);
                            }
                            configuracio.add(k, c);
                            obrirTresor(k+1, k1, k2+1);
                            passat.get(k1)[k2+1] = false;
                            if(modeVista == 0) {
                                m.deleteFromPath(k1, k2+1);
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            configuracio.remove(configuracio.size()-1);

                            break;

                        case 2:
                            if(modeVista == 0) {
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            c.setColumna(k2);
                            c.setFila(k1+1);
                            if(mapa.getMapa().get(k1+1)[k2] != 'T' && mapa.getMapa().get(k1+1)[k2] != 'E') {
                                c.setNumClaus(mapa.getMapa().get(k1+1)[k2]);
                            }
                            configuracio.add(k, c);
                            obrirTresor(k+1, k1+1, k2);
                            passat.get(k1+1)[k2] = false;
                            if(modeVista == 0) {
                                m.deleteFromPath(k1+1, k2);
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            configuracio.remove(configuracio.size()-1);

                            break;

                        case 3:
                            if(modeVista == 0) {
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            c.setColumna(k2-1);
                            c.setFila(k1);
                            if(mapa.getMapa().get(k1)[k2-1] != 'T' && mapa.getMapa().get(k1)[k2-1] != 'E') {
                                c.setNumClaus(mapa.getMapa().get(k1)[k2-1]);
                            }
                            configuracio.add(k, c);
                            obrirTresor(k+1, k1, k2-1);
                            passat.get(k1)[k2-1] = false;

                            if(modeVista == 0){
                                m.deleteFromPath(k1, k2-1);
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            configuracio.remove(configuracio.size()-1);
                            break;

                        case 4:
                            if(modeVista == 0) {
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            c.setColumna(k2);
                            c.setFila(k1-1);
                            if(mapa.getMapa().get(k1-1)[k2] != 'T' && mapa.getMapa().get(k1-1)[k2] != 'E') {
                                c.setNumClaus(mapa.getMapa().get(k1-1)[k2]);
                            }
                            configuracio.add(k, c);
                            obrirTresor(k+1, k1-1, k2);
                            passat.get(k1-1)[k2] = false;
                            if(modeVista == 0){
                                m.deleteFromPath(k1-1, k2);
                                m.setPathLength(configuracio.size());
                                m.setKeysCollected(num_claus);
                            }
                            configuracio.remove(configuracio.size()-1);
                            break;

                    }
                    if(modeVista == 0) {
                        //indiquem que NO hem passat per la casella
                        try {
                           Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }
    }


    public boolean solucio(int k1, int k2) {
        int maxK1 = mapa.getDimensioColumnes();
        int maxK2 = mapa.getDimensioFiles();
        if (mapa.getMapa().get(k1)[k2] == 'T' && k1 >= 0 && k1 < maxK1 && k2 >= 0 && k2 < maxK2) {
            return true;
        }
        return false;
    }


    /**
     * El metode factible ens indica si la solucio del backtracking es bona o no
     * @return true si es pot tractar la solucio, false si no es una solucio valida
     */
    public boolean factible() {
        int numClaus = 0;
        int max = configuracio.size();
        for(int i = 0; i < max; i++) {
            numClaus += configuracio.get(i).getNumClaus();
        }
        //Si hi ha suficient claus, podem obrir el tresor
        if(numClaus >= mapa.getClaus()) {
            return true;
        }
        return false;
    }

    /**
     * El metode completable indica si el backtracking pot continuar o si s'ha de podar
     * @param k1 nivell de l'arbre de cerca
     * @param k2 nivell de l'arbre de cerca
     * @return true si es pot continuar, false si s'ha de podar
     */
    public boolean completable(int k1, int k2, int direccio) {
        //Si no s'ha passat per aquella casella i no es una paret, podem avançar
        if(!foraRang(k1, k2, direccio) && !esParet(k1, k2, direccio) && !haPassat(k1, k2, direccio)) {
            return true;
        }
        return false;
    }

    public boolean haPassat(int k1, int k2, int direccio) {
        switch (direccio) {
            case 1:
                return passat.get(k1)[k2+1];
            case 2:
                return passat.get(k1+1)[k2];
            case 3:
                return passat.get(k1)[k2-1];
            case 4:
                return passat.get(k1-1)[k2];
        }
        return false;
    }

    public boolean foraRang(int k1, int k2, int direccio) {
        int maxK2 = mapa.getDimensioColumnes();
        int maxK1 = mapa.getDimensioFiles();
        switch (direccio) {
            case 1:
                return k2 == maxK2 - 1;
            case 2:
                return k1 == maxK1 - 1;
            case 3:
                return k2 == 0;
            case 4:
                return k1 == 0;
        }
        return false;
    }
    public boolean esParet(int k1, int k2, int direccio) {
        switch (direccio) {
            case 1:
                return mapa.getMapa().get(k1)[k2+1] == '-';
            case 2:
                return mapa.getMapa().get(k1+1)[k2] == '-';
            case 3:
                return mapa.getMapa().get(k1)[k2-1] == '-';
            case 4:
                return mapa.getMapa().get(k1-1)[k2] == '-';
        }
        return false;
    }
    /**
     * El metode tractarSolucio tracta la solucio trobada pel backtracking
     */
    public void tractarSolucio(int direccio) {
        int num_claus = 0;
        int dist = 0;
        int M = mapa.getDimensioColumnes();
        int N = mapa.getDimensioFiles();
        int aux;

        if(modeVista == 0) {
            for (int i = 0; i < v.getxMillor().size(); i++) {
                m2.deleteFromPath(v.getxMillor().get(i).getFila(), v.getxMillor().get(i).getColumna());
            }
        }

        for(int i = 0; i < N; i++) {
            for(int j = 0; j < M; j++) {
                //Si hem passat per la casella, no es ni el tresor ni l'entrada, sumem les claus
                if(mapa.getMapa().get(i)[j] != 'T' && mapa.getMapa().get(i)[j] != 'E' && passat.get(i)[j] == true) {
                    aux = Character.getNumericValue(mapa.getMapa().get(i)[j]);
                    num_claus += aux;
                }
            }
        }
        dist = configuracio.size();
        //Si ha recorrregut menys caselles
        if(dist < v.getDistMillor() && num_claus >= mapa.getClaus()) {
            v.setDistMillor(dist);

            v.setxMillor(configuracio);
            if(modeVista == 0) {
                actualitzaVista();
            }

        } else if (dist == v.getDistMillor() && num_claus >= mapa.getClaus()){
        //Si ha regut les mateixes caselles i menys claus acumulades
            if(num_claus < v.getClausMillor()) {
                v.setDistMillor(dist);
                v.setClausMillor(num_claus);
                v.setxMillor(configuracio);
                if(modeVista == 0) {
                    actualitzaVista();
                }
            }
        }

        if(modeVista == 0) {
            for (int i = 0; i < v.getxMillor().size(); i++) {
                m2.addToPath(v.getxMillor().get(i).getFila(), v.getxMillor().get(i).getColumna());
            }
        }
    }

    public MillorSolucio getV() {
        return v;
    }

    public void iniciaTemps() {
        startedAt = System.nanoTime();
    }

    public void paraTemps() {
        stopAt = System.nanoTime();
    }

    private void actualitzaVista() {
        m2.setKeysCollected(v.getClausMillor());
        m2.setPathLength(v.getDistMillor());
        paraTemps();
        long elapsedTime = stopAt - startedAt;
        m2.setElapsedTime(formatter2D.format(TimeUnit.NANOSECONDS.toHours(elapsedTime) % 24) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toMinutes(elapsedTime) % 60) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toSeconds(elapsedTime) % 60) +"." +
                formatter3D.format(TimeUnit.NANOSECONDS.toMillis(elapsedTime) % 1000));
    }
}

