/**
 *
 * Classe Casella
 *
 * S'encarrega de la informació que hi ha en cada casella del mapa
 *
 *
 */
public class Casella implements Cloneable{
    private int columna;
    private int fila;
    private int numClaus;

    public Casella() {
    }
    public Casella (int fila, int columna, int numClaus) {
        this.columna = columna;
        this. fila = fila;
        this. numClaus = numClaus;
    }
    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getNumClaus() {
        return numClaus;
    }

    public void setNumClaus(int numClaus) {
        this.numClaus = numClaus;
    }

    @Override
    public Object clone () {
        return new Casella(this.fila, this.columna, this.numClaus);
    }

    @Override
    public String toString () {
        return columna+"#"+fila;
    }
}
