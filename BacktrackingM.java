import GUI.MultiKeyTreasureGUI;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/*
Backtracking amb marcatge
 */
public class BacktrackingM {

    /* Atributs */
    private ArrayList<Casella> configuracio;
    private ArrayList<boolean[]> passat;
    private Fitxer mapa;
    private MillorSolucio v;
    private MultiKeyTreasureGUI m;
    private MultiKeyTreasureGUI m2;
    int modeVista;
    private DecimalFormat formatter2D;
    private DecimalFormat formatter3D;
    private long startedAt;
    private long stopAt;
    private int clausActuals;
    private int distanciaCami;


        public BacktrackingM(Fitxer mapa, MultiKeyTreasureGUI m, int modeVista) {
            clausActuals = 0;
            distanciaCami = -1; //-1 perque compta una casella de mes
            boolean[] aux;
            this.mapa = mapa;
            this.modeVista = modeVista;
            this.m = m;
            if(modeVista == 0){
                m2 = new MultiKeyTreasureGUI(350, 300, "M2", mapa.getMapaScenario());
            }
            configuracio = new ArrayList<Casella>();
            //Creem la matriu de booleans i la configuracio
            passat = new ArrayList<boolean[]>();
            for (int i = 0; i < mapa.getDimensioFiles(); i++) {
                aux = new boolean[mapa.getDimensioColumnes()];
                for (int j = 0; j < mapa.getDimensioColumnes(); j++) {
                    aux[j] = false;
                }
                passat.add(i, aux);
            }
            v = new MillorSolucio();
            this.formatter2D = new DecimalFormat("00");
            this.formatter3D = new DecimalFormat("000");

        }

        public void obrirTresor(int k, int k1, int k2) {
            //configuracio -> arraylist del recorregut
            //compt < 4
            int direccio = 0;
            //Marquem
            distanciaCami++;
            if(mapa.getMapa().get(k1)[k2] != 'T' && mapa.getMapa().get(k1)[k2] != 'E') {
                clausActuals += Character.getNumericValue(mapa.getMapa().get(k1)[k2]);
            }
            while (direccio < 4) {
                //seguentGerma
                direccio += 1;
                if (solucio(k1, k2)) {
                    if (factible()) {
                        tractarSolucio();
                    }

                } else {
                    if (completable(k1, k2, direccio) && distanciaCami <= v.getDistMillor()) {
                        passat.get(k1)[k2] = true;
                        if(modeVista == 0) {
                            //indiquem que hem passat per la casella
                            m.addToPath(k1, k2);
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            m.setKeysCollected(clausActuals);
                            m.setPathLength(distanciaCami);
                         }
                        Casella c = new Casella();
                        switch (direccio) {
                            case 1:
                                //passat.get(k1)[k2 + 1] = false;
                                c.setColumna(k2+1);
                                c.setFila(k1);
                                if (mapa.getMapa().get(k1)[k2 + 1] != 'T' && mapa.getMapa().get(k1)[k2 + 1] != 'E') {
                                    c.setNumClaus(mapa.getMapa().get(k1)[k2 + 1]);
                                }
                                configuracio.add(k, c);
                                obrirTresor(k+1, k1, k2 + 1);
                                passat.get(k1)[k2 + 1] = false;
                                //Desmarquem
                                if (mapa.getMapa().get(k1)[k2 + 1] != 'T' && mapa.getMapa().get(k1)[k2 + 1] != 'E') {

                                    clausActuals -= Character.getNumericValue(mapa.getMapa().get(k1)[k2 + 1]);
                                }
                                configuracio.remove(configuracio.size()-1);

                                distanciaCami--;
                                if(modeVista == 0){
                                    m.deleteFromPath(k1, k2 + 1);
                                    m.setKeysCollected(clausActuals);
                                    m.setPathLength(distanciaCami);
                                }
                                break;

                            case 2:
                                //passat.get(k1 + 1)[k2] = false;
                                c.setColumna(k2);
                                c.setFila(k1+1);
                                if (mapa.getMapa().get(k1 + 1)[k2] != 'T' && mapa.getMapa().get(k1 + 1)[k2] != 'E') {
                                    c.setNumClaus(mapa.getMapa().get(k1 + 1)[k2]);
                                }
                                configuracio.add(k, c);
                                obrirTresor(k+1, k1 + 1, k2);
                                passat.get(k1 + 1)[k2] = false;

                                //Desmarquem
                                if (mapa.getMapa().get(k1 + 1)[k2] != 'T' && mapa.getMapa().get(k1 + 1)[k2] != 'E') {

                                    clausActuals -= Character.getNumericValue(mapa.getMapa().get(k1 + 1)[k2]);
                                }
                                configuracio.remove(configuracio.size()-1);

                                distanciaCami--;
                                if(modeVista == 0) {
                                    m.deleteFromPath(k1 + 1, k2);
                                    m.setKeysCollected(clausActuals);
                                    m.setPathLength(distanciaCami);
                                }
                                break;

                            case 3:
                                //passat.get(k1)[k2 - 1] = false;
                                c.setColumna(k2-1);
                                c.setFila(k1);
                                if (mapa.getMapa().get(k1)[k2 - 1] != 'T' && mapa.getMapa().get(k1)[k2 - 1] != 'E') {
                                    c.setNumClaus(mapa.getMapa().get(k1)[k2 - 1]);
                                }
                                configuracio.add(k, c);
                                obrirTresor(k+1,k1, k2 - 1);
                                passat.get(k1)[k2 - 1] = false;

                                //Desmarquem
                                if (mapa.getMapa().get(k1)[k2 - 1] != 'T' && mapa.getMapa().get(k1)[k2 - 1] != 'E') {

                                    clausActuals -= Character.getNumericValue(mapa.getMapa().get(k1)[k2 - 1]);
                                }
                                configuracio.remove(configuracio.size()-1);

                                distanciaCami--;
                                if(modeVista == 0) {
                                    m.deleteFromPath(k1, k2 - 1);
                                    m.setKeysCollected(clausActuals);
                                    m.setPathLength(distanciaCami);
                                }
                                break;

                            case 4:
                                //passat.get(k1 - 1)[k2] = false;
                                c.setColumna(k2);
                                c.setFila(k1-1);
                                if (mapa.getMapa().get(k1 - 1)[k2] != 'T' && mapa.getMapa().get(k1 - 1)[k2] != 'E') {
                                    c.setNumClaus(mapa.getMapa().get(k1 - 1)[k2]);
                                }
                                configuracio.add(k, c);
                                obrirTresor(k+1, k1 - 1, k2);
                                passat.get(k1 - 1)[k2] = false;
                                //Desmarquem
                                if (mapa.getMapa().get(k1 - 1)[k2] != 'T' && mapa.getMapa().get(k1 - 1)[k2] != 'E') {
                                    clausActuals -= Character.getNumericValue(mapa.getMapa().get(k1 - 1)[k2]);
                                }
                                configuracio.remove(configuracio.size()-1);

                                distanciaCami--;
                                if(modeVista == 0) {
                                    m.deleteFromPath(k1 - 1, k2);
                                    m.setKeysCollected(clausActuals);
                                    m.setPathLength(distanciaCami);
                                }
                                break;

                        }
                        //indiquem que NO hem passat per la casella
                        if(modeVista == 0) {
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                              e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }


        public boolean solucio(int k1, int k2) {
            int maxK1 = mapa.getDimensioColumnes();
            int maxK2 = mapa.getDimensioFiles();
            if (mapa.getMapa().get(k1)[k2] == 'T' && k1 >= 0 && k1 < maxK1 && k2 >= 0 && k2 < maxK2) {
                return true;
            }
            return false;
        }


        /**
         * El metode factible ens indica si la solucio del backtracking es bona o no
         *
         * @return true si es pot tractar la solucio, false si no es una solucio valida
         */
        public boolean factible() {
            int max = configuracio.size();
            if (clausActuals >= mapa.getClaus()) {
                return true;
            } else {
               return false;
            }
        }

        /**
         * El metode completable indica si el backtracking pot continuar o si s'ha de podar
         *
         * @param k1 nivell de l'arbre de cerca
         * @param k2 nivell de l'arbre de cerca
         * @return true si es pot continuar, false si s'ha de podar
         */
        public boolean completable(int k1, int k2, int direccio) {
            //Si no s'ha passat per aquella casella i no es una paret, podem avançar
            if (!foraRang(k1, k2, direccio) && !esParet(k1, k2, direccio) && !haPassat(k1, k2, direccio)) {
                return true;
            }
            return false;
        }

        public boolean haPassat(int k1, int k2, int direccio) {
            switch (direccio) {
                case 1:
                    return passat.get(k1)[k2 + 1];
                case 2:
                    return passat.get(k1 + 1)[k2];
                case 3:
                    return passat.get(k1)[k2 - 1];
                case 4:
                    return passat.get(k1 - 1)[k2];
            }
            return false;
        }

        public boolean foraRang(int k1, int k2, int direccio) {
            int maxK2 = mapa.getDimensioColumnes();
            int maxK1 = mapa.getDimensioFiles();
            switch (direccio) {
                case 1:
                    return k2 == maxK2 - 1;
                case 2:
                    return k1 == maxK1 - 1;
                case 3:
                    return k2 == 0;
                case 4:
                    return k1 == 0;
            }
            return false;
        }

        public boolean esParet(int k1, int k2, int direccio) {
            switch (direccio) {
                case 1:
                    return mapa.getMapa().get(k1)[k2 + 1] == '-';
                case 2:
                    return mapa.getMapa().get(k1 + 1)[k2] == '-';
                case 3:
                    return mapa.getMapa().get(k1)[k2 - 1] == '-';
                case 4:
                    return mapa.getMapa().get(k1 - 1)[k2] == '-';
            }
            return false;
        }

        /**
         * El metode tractarSolucio tracta la solucio trobada pel backtracking
         */
        //Canviar tractarSolucio -> utilitzar la nova config (array de caselles)
        public void tractarSolucio() {

            if(modeVista == 0) {
                for (int i = 0; i < v.getxMillor().size(); i++) {
                    m2.deleteFromPath(v.getxMillor().get(i).getFila(), v.getxMillor().get(i).getColumna());
                }
            }
            //Si ha recorrregut menys caselles
            if (distanciaCami < v.getDistMillor()) {
                v.setDistMillor(distanciaCami);
                v.setClausMillor(clausActuals);
                v.setxMillor(configuracio);
                if(modeVista == 0) {
                    actualitzaVista();
                }
            }
            //Si ha regut les mateixes caselles i menys claus acumulades
            if (distanciaCami == v.getDistMillor()) {
                if (clausActuals < v.getClausMillor()) {
                    v.setDistMillor(distanciaCami);
                    v.setClausMillor(clausActuals);
                    v.setxMillor(configuracio);
                    if(modeVista == 0) {
                        actualitzaVista();
                    }
                }
            }
            if(modeVista == 0) {
                for (int i = 0; i < v.getxMillor().size(); i++) {
                    m2.addToPath(v.getxMillor().get(i).getFila(), v.getxMillor().get(i).getColumna());
                }
            }
        }

    public MillorSolucio getV() {
        return v;
    }

    public void iniciaTemps() {
        startedAt = System.nanoTime();
    }

    public void paraTemps() {
        stopAt = System.nanoTime();
    }

    private void actualitzaVista() {
        m2.setKeysCollected(v.getClausMillor());
        m2.setPathLength(v.getDistMillor());
        paraTemps();
        long elapsedTime = stopAt - startedAt;
        m2.setElapsedTime(formatter2D.format(TimeUnit.NANOSECONDS.toHours(elapsedTime) % 24) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toMinutes(elapsedTime) % 60) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toSeconds(elapsedTime) % 60) +"." +
                formatter3D.format(TimeUnit.NANOSECONDS.toMillis(elapsedTime) % 1000));
    }
}


