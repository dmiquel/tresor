import java.io.*;
import java.util.ArrayList;

/**
 *
 * Fitxer
 *
 * Classe que s'encarrega de llegir el fitxer del mapa i guardar-ho tot.
 *
 */
public class Fitxer {

    private int claus;
    private ArrayList<char[]> mapa;
    private String[][] scenario;
    private int k1;
    private int k2;

    public void llegeixFitxer(String path) throws IOException{
        String files;
        mapa = new ArrayList<char[]>();

        try {
            Reader reader = new FileReader("../" + path);
            BufferedReader b = new BufferedReader(reader);
            claus = Integer.valueOf(b.readLine());
            while ((files = b.readLine()) != null) {
                mapa.add(files.toCharArray());
            }
            b.close();

        }catch(FileNotFoundException e) {
            System.out.println("Error en la lectura del fitxer");

        }
    }

    public int getClaus() {
        return claus;
    }

    public ArrayList<char[]> getMapa() {
        return mapa;
    }

    public int getDimensioColumnes () {
        return mapa.get(1).length;
    }

    public int getDimensioFiles() {
        return mapa.size();
    }
    public String[][] getMapaScenario() {
        int files = getDimensioFiles();
        int columnes = getDimensioColumnes();
        scenario = new String[files][columnes];

        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes; j++) {
                scenario[i][j] = String.valueOf(mapa.get(i)[j]);
            }
        }
        return scenario;
    }

    public Casella trobaEntrada() {
        Casella entrada = new Casella();
        for (int i = 0; i < mapa.size(); i++) {
            for (int j = 0; j < mapa.get(i).length; j++) {
                if(mapa.get(i)[j] == 'E') {
                    entrada.setColumna(j);
                    entrada.setFila(i);
                    entrada.setNumClaus(0);
                    k1 = i;
                    k2 = j;
                    break;
                }
            }
        }
        return entrada;
    }


    public Casella trobaTresor() {
        Casella tresor = new Casella();
        for (int i = 0; i < mapa.size(); i++) {
            for (int j = 0; j < mapa.get(i).length; j++) {
                if(mapa.get(i)[j] == 'T') {
                    tresor.setColumna(i);
                    tresor.setFila(j);
                    tresor.setNumClaus(0);
                    break;
                }
            }
        }
        return tresor;
    }

    public int getK1() {
        return k1;
    }

    public int getK2() {
        return k2;
    }
}
