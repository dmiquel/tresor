import sun.util.resources.cldr.uz.CalendarData_uz_Latn_UZ;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by joanfito on 2/1/17.
 */
public class Configuracio implements Comparable<Configuracio>, Cloneable{

    public ArrayList<Casella> x;
    private int k1;
    private int k2;
    private ArrayList<boolean[]> passat;
    private double heuristica;
    private Casella tresor;
    public int clausActuals;
    public int distanciaCami;


    public Configuracio(int files, int columnes, Casella tresor) {
        k1 = 0;
        k2 = 0;
        boolean[] aux;
        passat = new ArrayList<boolean[]>();
        x = new ArrayList<>();
        for (int i = 0; i < files; i++) {
            aux = new boolean[columnes];
            for (int j = 0; j < columnes; j++) {
                aux[j] = false;
            }
            passat.add(i,aux);
        }
        this.tresor = tresor;

    }

    public Configuracio(Configuracio conf, Casella cas) {
        this.x = (ArrayList)conf.x.clone();
        x.add((Casella)cas.clone());
        this.tresor = conf.tresor;
        heuristica = valorEstimat(tresor);
        passat = (ArrayList)conf.passat.clone();
        passat.get(cas.getFila())[cas.getColumna()] = true;
        this.clausActuals = conf.clausActuals;
        this.distanciaCami = conf.distanciaCami;
    }

    private double valorEstimat(Casella tresor) {
        //Primera heuristica -> sabem on es troba el tresor
        //Mirem per quantes caselles hem passat
        Casella actual = new Casella();
        actual =  x.get(x.size()-1);
        int diferenciaX = 0;
        int diferenciaY = 0;
        //System.out.println(tresor);
        diferenciaX = tresor.getColumna() - actual.getColumna();
        diferenciaY = tresor.getFila() - actual.getFila();
        return Math.sqrt(Math.pow(diferenciaX, 2) + Math.pow(diferenciaY, 2));
    }
    public Configuracio (ArrayList<Casella> c, int k1, int k2, ArrayList<boolean[]> b, Casella tresor) {
        this.x = (ArrayList<Casella>)c.clone();
        this.k1 = k1;
        this.k2 = k2;
        this.passat = (ArrayList<boolean[]>)b.clone();
        this.tresor = tresor;
        heuristica = valorEstimat(tresor);
    }
    public int getMida() {
        return x.size();
    }
    public Casella getX(int index) {
        return x.get(index);
    }
    public int getConfiguracioSize () {return x.size();}

    public void setX( Casella casella) {
        x.add(casella);
    }

    public int getK1() {
        return k1;
    }

    public void setK1(int k1) {
        this.k1 = k1;
    }

    public int getK2() {
        return k2;
    }

    public void setK2(int k2) {
        this.k2 = k2;
    }

    public Casella getCasellaActual() {
        int index = x.size()-1;
        return x.get(index);
    }

    public ArrayList<boolean[]> getPassat() {
        return passat;
    }

    public void setPassat(ArrayList<boolean[]> passat) {
        this.passat = passat;
    }


    @Override
    public Object clone () {

        return new Configuracio(this.x, this.k1, this.k2, this.passat, this.tresor);
    }

    public void afegirCasella (Casella c) {
        x.add(c);
        passat.get(c.getFila())[c.getColumna()] = true;
    }

    @Override
    public int compareTo(Configuracio o) {
        if (o.heuristica > this.heuristica) {
            return 1;
        } else if (o.heuristica < this.heuristica) {
            return 0;
        } else {
            return -1;
        }
    }

    public int obtenirClaus() {
        int numClaus = 0;
        int max = x.size();
        for(int j = 0; j < max; j++) {
            numClaus += x.get(j).getNumClaus();
            System.out.println("Num "+x.get(j).getNumClaus());
        }
        return numClaus;
    }
}
