import GUI.MultiKeyTreasureGUI;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by joanfito on 2/1/17.
 */


public class BranchAndBoundM {
    private PriorityQueue<Configuracio> nodesVius;
    private ArrayList<Configuracio> fills;
    private Fitxer mapa;
    private MillorSolucio v;
    private MultiKeyTreasureGUI m;
    private MultiKeyTreasureGUI m2;
    int modeVista;
    private int direccio;
    private DecimalFormat formatter2D;
    private DecimalFormat formatter3D;
    private long startedAt;
    private long stopAt;

    /* Metodes */

    public BranchAndBoundM(Fitxer mapa, MultiKeyTreasureGUI m, int modeVista) {
        this.mapa = mapa;
        this.modeVista = modeVista;
        if(modeVista == 0) {
            m2 = new MultiKeyTreasureGUI(350, 300, "M2", mapa.getMapaScenario());
        }
        this.m = m;
        direccio = 0;
        nodesVius = new PriorityQueue<>();
        this.formatter2D = new DecimalFormat("00");
        this.formatter3D = new DecimalFormat("000");

    }

    public Configuracio obrirTresor() {
        Configuracio x, xMillor;
        xMillor = new Configuracio(0, 0, mapa.trobaTresor());
        int vMillorCami = 0, vMillorClaus = 0, numFills = 0;

        //Cua Prioritat creada en el constructor
        x = configuracioArrel();
        nodesVius.add(x);


        vMillorCami = 60;
        vMillorClaus = Integer.MAX_VALUE;

        while (!nodesVius.isEmpty()) {
            if(modeVista == 0) {
                m.deletePath();
            }
            x = nodesVius.poll();

            numFills = expandeix(x);
            for (int i = 0; i < numFills; i++) {
                if (solucio(fills.get(i))) {
                    if (factible(fills.get(i))) {
                        if (valor(fills.get(i)) < vMillorCami) {
                            if(modeVista == 0) {
                                for(int k = 0; k < xMillor.getConfiguracioSize(); k++) {
                                    m2.deleteFromPath(xMillor.getX(k).getFila(), xMillor.getX(k).getColumna());
                                }
                            }

                            //Si el cami es mes curt
                            vMillorCami = valor(fills.get(i));
                            vMillorClaus = fills.get(i).clausActuals;
                            xMillor = fills.get(i);

                            if(modeVista == 0) {
                                actualitzaVista(xMillor, vMillorCami, vMillorClaus);
                            }

                        } else if (valor(fills.get(i)) == vMillorCami) {
                            //Si el cami es igual, mirem el numero de clau
                            if (fills.get(i).clausActuals < vMillorClaus) {
                                if(modeVista == 0) {
                                    for(int k = 0; k < xMillor.getConfiguracioSize(); k++) {
                                        m2.deleteFromPath(xMillor.getX(k).getFila(), xMillor.getX(k).getColumna());
                                    }
                                }
                                vMillorCami = valor(fills.get(i));
                                xMillor = fills.get(i);
                                vMillorClaus = fills.get(i).clausActuals;
                                if(modeVista == 0) {
                                    actualitzaVista(xMillor, vMillorCami, vMillorClaus);
                                }
                            }
                        }
                    }
                } else {
                    //Si no es solucio
                    if (completable(fills.get(i), direccio)) {

                        if (valorParcial(fills.get(i)) <= vMillorCami) {
                            nodesVius.add(fills.get(i));
                        }
                    }
                }
            }
        }
        return xMillor;
    }

    public Configuracio configuracioArrel() {
        Configuracio c = new Configuracio(6, 10, mapa.trobaTresor());
        c.afegirCasella(mapa.trobaEntrada());
        return c;
    }

    /*
    Retorna el enter, la configuracio es un atribut de la classe
     */
    public int expandeix(Configuracio c) {
        int i = 0;
        fills = new ArrayList<>();
        Casella casella = new Casella();
        Casella ultima = c.x.get(c.x.size() - 1);
        Configuracio aux;
        if(modeVista == 0) {
            for (int k = 0; k < c.getConfiguracioSize(); k++) {
                m.addToPath(c.getX(k).getFila(),c.getX(k).getColumna());

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        for (i = 1; i < 5; i++) {
            switch (i) {
                case 1:
                    if (!foraRang(c, ultima.getFila(), ultima.getColumna() + 1) && !esParet(c, ultima.getFila(), ultima.getColumna() + 1) && !haPassat(c, ultima.getFila(), ultima.getColumna() + 1)) {
                        casella.setColumna(ultima.getColumna() + 1);
                        casella.setFila(ultima.getFila());
                        aux = new Configuracio(c, casella);
                        if (mapa.getMapa().get(ultima.getFila())[ultima.getColumna() + 1] != 'T' && mapa.getMapa().get(ultima.getFila())[ultima.getColumna() + 1] != 'E') {
                            casella.setNumClaus(Character.getNumericValue(
                                    mapa.getMapa().get(ultima.getFila())[ultima.getColumna()+1]));
                            //Marquem
                            aux.distanciaCami = aux.getConfiguracioSize();
                            aux.clausActuals += casella.getNumClaus();
                            if(modeVista == 0) {
                                m.setKeysCollected(c.clausActuals);
                                m.setPathLength(c.distanciaCami);
                            }
                        }
                        fills.add(aux);

                    }
                    break;

                case 2:

                    if (!foraRang(c, ultima.getFila() + 1, ultima.getColumna()) && !esParet(c,
                            ultima.getFila() + 1, ultima.getColumna()) &&
                            !haPassat(c, ultima.getFila() + 1, ultima.getColumna())) {
                        casella.setColumna(ultima.getColumna());
                        casella.setFila(ultima.getFila() + 1);
                        aux = new Configuracio(c, casella);
                        if (mapa.getMapa().get(ultima.getFila() + 1)[ultima.getColumna()] != 'T' && mapa.getMapa().get(ultima.getFila() + 1)[ultima.getColumna()] != 'E') {
                            casella.setNumClaus(Character.getNumericValue(
                                    mapa.getMapa().get(ultima.getFila()+1)[ultima.getColumna()]));
                            //Marquem
                            aux.distanciaCami = aux.getConfiguracioSize();
                            aux.clausActuals += casella.getNumClaus();
                            if(modeVista == 0) {
                                m.setKeysCollected(c.clausActuals);
                                m.setPathLength(c.distanciaCami);
                            }
                        }
                        fills.add(aux);

                    }

                    break;
                case 3:
                    if (!foraRang(c, ultima.getFila(), ultima.getColumna() - 1) && !esParet(c, ultima.getFila(),
                            ultima.getColumna() - 1) && !haPassat(c, ultima.getFila(), ultima.getColumna() - 1)) {
                        casella.setColumna(ultima.getColumna() - 1);
                        casella.setFila(ultima.getFila());
                        aux = new Configuracio(c, casella);
                        if (mapa.getMapa().get(ultima.getFila())[ultima.getColumna() - 1] != 'T' && mapa.getMapa().get(ultima.getFila())[ultima.getColumna() - 1] != 'E') {
                            casella.setNumClaus(Character.getNumericValue(
                                    mapa.getMapa().get(ultima.getFila())[ultima.getColumna()-1]));
                            //Marquem
                            aux.distanciaCami = aux.getConfiguracioSize();
                            aux.clausActuals += casella.getNumClaus();
                            if(modeVista == 0) {
                                m.setKeysCollected(c.clausActuals);
                                m.setPathLength(c.distanciaCami);
                            }
                        }
                        fills.add(aux);

                    }
                    break;
                case 4:
                    if (!foraRang(c, ultima.getFila() - 1, ultima.getColumna()) && !esParet(c, ultima.getFila() - 1, ultima.getColumna()) && !haPassat(c, ultima.getFila() - 1, ultima.getColumna())) {
                        casella.setColumna(ultima.getColumna());
                        casella.setFila(ultima.getFila() - 1);
                        aux = new Configuracio(c, casella);
                        if (mapa.getMapa().get(ultima.getFila() - 1)[ultima.getColumna()] != 'T' && mapa.getMapa().get(ultima.getFila() - 1)[ultima.getColumna()] != 'E') {
                            casella.setNumClaus(Character.getNumericValue(
                                    mapa.getMapa().get(ultima.getFila()-1)[ultima.getColumna()]));
                            //Marquem
                            aux.distanciaCami = aux.getConfiguracioSize();
                            aux.clausActuals += casella.getNumClaus();
                            if(modeVista == 0) {
                                m.setKeysCollected(c.clausActuals);
                                m.setPathLength(c.distanciaCami);
                            }
                        }
                        fills.add(aux);

                    }
                    break;
            }
        }
        return fills.size();
    }

    //valor seria nomes distancia o tambe numero de claus?
    public int valor(Configuracio c) {
        return c.distanciaCami;
    }

    public int valorParcial(Configuracio c) {
        return c.distanciaCami;
    }


    public boolean completable(Configuracio c, int direccio) {
        //Si no s'ha passat per aquella casella i no es una paret, podem avançar
        return true;
    }

    public boolean haPassat(Configuracio c, int x, int y) {
        for(int i = 0; i < c.getConfiguracioSize(); i++) {
            if(c.getX(i).getFila() == x && c.getX(i).getColumna() == y) {
                return true;
            }
        }
        return false;
    }

    public boolean foraRang(Configuracio c, int x, int y) {
        int maxY = mapa.getDimensioColumnes();
        int maxX = mapa.getDimensioFiles();
        if (x >= 0 && x < maxX && y >= 0 && y < maxY) {
            return false;
        }
        return true;
    }

    public boolean esParet(Configuracio c, int x, int y) {
        return mapa.getMapa().get(x)[y] == '-';
    }

    public boolean factible(Configuracio c) {
        //Si hi ha suficient claus, podem obrir el tresor
        if (c.clausActuals >= mapa.getClaus()) {
            return true;
        }

        return false;
    }

    public boolean solucio(Configuracio c) {
        int maxK1 = mapa.getDimensioColumnes();
        int maxK2 = mapa.getDimensioFiles();
        Casella ultima = c.x.get(c.x.size() - 1);
        if (mapa.getMapa().get(ultima.getFila())[ultima.getColumna()] == 'T' &&
                !foraRang(c, ultima.getFila(), ultima.getColumna())) {

            return true;
        }
        return false;
    }

    public void iniciaTemps() {
        startedAt = System.nanoTime();
    }

    public void paraTemps() {
        stopAt = System.nanoTime();
    }

    private void actualitzaVista(Configuracio xMillor, int vMillorCami,int vMillorClaus) {
        for(int k = 0; k < xMillor.getConfiguracioSize(); k++) {
            m2.addToPath(xMillor.getX(k).getFila(), xMillor.getX(k).getColumna());
        }
        m2.setKeysCollected(vMillorClaus);
        m2.setPathLength(vMillorCami);
        paraTemps();
        long elapsedTime = stopAt - startedAt;
        m2.setElapsedTime(formatter2D.format(TimeUnit.NANOSECONDS.toHours(elapsedTime) % 24) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toMinutes(elapsedTime) % 60) + ":" +
                formatter2D.format(TimeUnit.NANOSECONDS.toSeconds(elapsedTime) % 60) +"." +
                formatter3D.format(TimeUnit.NANOSECONDS.toMillis(elapsedTime) % 1000));
    }
}
