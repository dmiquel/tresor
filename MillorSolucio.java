import java.util.ArrayList;

/**
 *
 * Classe MillorSolucio
 *
 * S'encarrega de...
 *
 */
public class MillorSolucio {

    /* Atribut */

    private ArrayList<Casella> xMillor;
    private int distMillor;
    private int clausMillor;

    /* Metodes */

    public MillorSolucio() {
        xMillor = new ArrayList<>();
        distMillor = 60; //numero total de caselles
        clausMillor = Integer.MAX_VALUE;
    }

    /**
     * El metode getxMillor dona el valor de l'atribut xMillor
     * @return la millor configuracio solucio
     */
    public ArrayList<Casella> getxMillor() {
        return xMillor;
    }

    /**
     * El metode getDistMillor dona el valor de l'atribut distMillor
     * @return la distancia de la millor configuracio
     */
    public int getDistMillor() {
        return distMillor;
    }

    /**
     * El metode getClausMillor dona el valor de l'atribut clausMillor
     * @return el numero de claus obtingudes en la millor configuracio
     */
    public int getClausMillor() {
        return clausMillor;
    }

    /**
     * El metode setxMillor assigna un valor a l'atribut xMillor
     * @param xMillor la nova millor configuracio
     */
    public void setxMillor(ArrayList<Casella> xMillor) {
        this.xMillor = (ArrayList<Casella>)xMillor.clone();
    }

    /**
     * El metode setDistMillor assigna un valor a l'atribut distMillor
     * @param distMillor la distancia de la nova millor configuracio
     */
    public void setDistMillor(int distMillor) {
        this.distMillor = distMillor;
    }

    /**
     * El metode setClausMillor assigna un valor a l'atribut clausMillor
     * @param clausMillor el numero de claus de la nova millor configuracio
     */
    public void setClausMillor(int clausMillor) {
        this.clausMillor = clausMillor;
    }
}
